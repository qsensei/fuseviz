# FuseViz

Drop-in visualisation toolkit for the Fuse platform.

## Dependencies

- [C3](http://www.c3js.org)
- [D3](http://www.d3js.org)
- [Lodash](http://www.lodash.com)

## Usage

1) [Download](https://bitbucket.org/jpablolomeli/fuseviz/downloads/fuseviz-0.1.0.zip) the toolkit.

2) Unzip the file, you'll get the files in the following direcotry structure:

![Directory structure](https://bytebucket.org/jpablolomeli/fuseviz/raw/5527c2a35151259621a720a29db4b06148e58108/images/dir_structure.jpg?token=f4dcef45a606f86f040f32894f0355c89fa2f268)

3) Include the references to the files in your html, modify according to your own structure:

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="vendor/css/c3.css">
</head>
<body>
	<script src="js/vendor/lodash.js"></script>
	<script src="js/vendor/d3.js"></script>
	<script src="js/vendor/c3.js"></script>
	<script src="js/FuseViz.js"></script>
</body>
</html>
```

4) Profit!

```
<div id="chart"></div>

<!-- <script> references... -->
<script>
config = {
  type: 'line',
  selector: 'chart',
  data: dataObj,
  gridX: false,
  gridY: true,
  zoom: false,
  tooltip: true,
  typeSwitcher: 'topRight',
  colorScheme: '20',
  oneColor: true,
  legend: true
};

fuseInstance = new FuseViz(config);
</script>
	
```

5) As on optimization you are encouraged to concatenate and minify your script files using something like uglify. 

## API

### Initialize

`var fuseVizInstance = new fuseViz(config);`

```
config = {
  type: 'bar',                // (string) 'pie', 'line', 'area', 'area-spline', 'donut'. Default: 'bar'
  selector: 'chart',          // (string) id of the DOM Element where to draw the chart,
  data: dataObj || url,       // (obj) data to graph in JSON || (string) url to make an Ajax call to fuse
  legend: true,               // (bool) Show or hide the legend (label) for the dataset on the chart. Default: true
  legendPosition: 'bottom',   // (string) Position the legend on 'bottom' or 'right'. Default: 'bottom'
  labels: false,              // (bool) Show a label with the value on top of a point or bar. Default: false
  gridX: true,                // (bool) Draw x axis grid (only affects graphs with axes like, bar, area, line). Default: false
  gridY: true,                // (bool) Draw y axis grid (only affects graphs with axes like, bar, area, line). Default: false
  zoom: true,                 // (bool) Enable zoom and pan in chart (only affects graphs with axes like, bar, area, line). Default: false
  tooltip: true,              // (bool) Enable tooltips on data hover,
  typeSwitcher: 'topRight',   // (string) 'topLeft'. Enable a switcher to change chart type. Default: topRight
  colorScheme: '10',          // (string) Choose a d3 color scheme ('10', '20', '20b', '20c'). Default: '20'
  oneColor: true              // (bool) Make a dataset show in one color. Default: false
};
```

-----------------------------------------------------------

### Change chart type

`fuseVizInstance.changeChartType(args);`

```
  => args
  type: (string) 'bar', 'pie', 'line', 'area', 'area-spline', 'donut'
```

Example:

`fuseVizInstance.changeChartType('pie');`

-----------------------------------------------------------

### Update chart (new data)

`fuseVizInstance.update(data);`

```
  => args
  data: (object) json data from Fuse || (string) url to fetch data from fuse
```

-----------------------------------------------------------

### Load dataset in chart

`fuseVizInstance.loadData(data, type);`

```
=> args
data: (object) json data from Fuse || (string) url to fetch data from fuse
type: (string) Type of chart to draw the new dataset in
```

**Note:**
If there are multiple datasets in one chart the method 'changeChartType' won't have any effect, and the typeSwitcher won't be added.

----

### Set a date range in chart

`fuseVizInstance.setRange(dateFrom, dateTo);`

```
=> args

rangeFrom: (string) A valid 'from' date string.
rangeTo: (string) A valid 'to' date string.
```

Example:

`fuseVizInstance.setRange('2014-04-11', '2014-05-20');`

---

### Reset range (restore with original data)

`fuseVizInstance.resetRange();`

---

### Get current instance configuration

`fuseVizInstance.getConfig();`

---

## Maps

To graph with maps, use [FuseMap](https://bitbucket.org/jpablolomeli/fusemap/)

---

##Changelog

#### 18 Aug 2014
**v0.1.0**
- Initial version made available!