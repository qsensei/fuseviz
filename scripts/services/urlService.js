var urlService = {
  getFacets: 'http://labs.qsensei.com:18010/facets',
  getFacetsProd: 'http://atlassian.qsensei.com:2080/facets',
  base: 'http://atlassian.qsensei.com:2080/',
  twc: 'data/twc_sample.json',
  jason: 'data/facets.jason.json'
};