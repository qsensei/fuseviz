var webServices = (function() {
  'use strict';

  var getAllFacets = function() {
    return $.get(urlService.getFacets);
  };

  var getFacet = function(query) {
    return $.get(urlService.getFacets + '/' + query);
  };

  var getTWCSample = function() {
    return $.get(urlService.twc);
  };

  var getJasonSample = function() {
    return $.get(urlService.jason);
  };

  var getFacetsProd = function(query) {
    return query ? $.get(urlService.getFacetsProd + '/' + query) : $.get(urlService.getFacetsProd);
  };

  var queryFuse = function(query) {
    return query ? $.get(urlService.base + query) : $.get(urlService.base);
  };



  return {
    getAllFacets: getAllFacets,
    getFacet: getFacet,
    getFacetsProd: getFacetsProd,
    queryFuse: queryFuse,
    getTWCSample: getTWCSample,
    getJasonSample: getJasonSample
  };

})();
