'use strict';

var FuseViz = function(config) {

  // a) Verify if required properties have been provided and required libraries are present
  // ---------------------------------------------------

  this.verify(config);

  // b) Configure
  // ---------------------------------------------------
  // 1. User configuration
  this.selector = config.selector;
  this.gridX = _.isUndefined(config.gridX) ? false : config.gridX;
  this.gridY = _.isUndefined(config.gridY) ? false : config.gridY;
  this.zoom = _.isUndefined(config.zoom) ? false : config.zoom;
  this.labels = _.isUndefined(config.labels) ? false : config.labels;
  this.oneColor = _.isUndefined(config.oneColor) ? false : config.oneColor;
  this.tooltip = _.isUndefined(config.tooltip) ? false : config.tooltip;
  this.subchart = _.isUndefined(config.subchart) ? false : config.subchart;
  this.chartType = config.type || 'bar';
  this.legend = _.isUndefined(config.legend) ? true : config.legend;
  this.legendPosition = config.legendPosition || 'bottom';
  this.colorScheme = config.colorScheme || '20';
  this.typeSwitcher = _.isUndefined(config.typeSwitcher) ? false : config.typeSwitcher;
  this.xs = {};
  this.xType = 'category';
  this.multipleTypes = {};

  // Check if we have url or data and initialise
  if (_.isObject(config.data)) {
    this.initialise(config.data);
  }
  else if (_.isString(config.data)) {

    var that = this;
    var url = config.data;

    d3.json(url, function(data) {
      that.initialise(data);
    });

  }

};

/**
 * Method to throw Exceptions
 * @param {obj} value   The value that causes the exception
 * @param {string} message The message to display
 */
FuseViz.prototype.Exception = function(value, message) {
  this.value = value;
  this.message = message;
  this.toString = function() {
    return this.value + ': ' + this.message;
  };
};

/**
 * Method to verify if required properties have been provided and required libraries are present
 * @param  {[obj]} config The configuration object
 */
FuseViz.prototype.verify = function(config) {
  if (_.isUndefined(config.selector)) {
    throw new this.Exception(config.selector, 'no selector was provided');
  }

  if (_.isUndefined(config.data)) {
    throw new this.Exception(config.data, 'no data was provided');
  }

  if (_.isUndefined(window.d3)) {
    throw new this.Exception(window.d3, 'D3 is not present and is a dependency of the toolkit, please include it');
  }

  if (_.isUndefined(window.c3)) {
    throw new this.Exception(window.c3, 'C3 is not present and is a dependency of the toolkit, please include it');
  }
};

/**
 * Constructor method
 * @param  {obj} data Fetched or provided data
 */
FuseViz.prototype.initialise = function(data) {
  this.rawData = data;
  this.facet = this.rawData.label;
  // c) Initialise
  // ---------------------------------------------------

  // 1. Parse the data
  var parsedData = this.parseData(this.rawData);
  this.currentData = parsedData;

  // 2. Graph
  this.graph(parsedData);
};

/**
 * Helper method to guess if facet is Date
 * TODO: In future we should consider adding 'type' to Indices in the indexschema
 * @param {Array} X-Axis Data Array
 */
FuseViz.prototype._isDateFacet = function(rawData) {
  return false;
  var xLabel = rawData.label;
  var firstValue = _.first(rawData.items).value;
  var doesDateParse = ! isNaN(new Date(firstValue).getTime());
  return doesDateParse || _.contains(xLabel.label, 'date') || _.contains(xLabel.label, 'Date') ;

}

/**
 * Method that takes Fuse's raw JSON data and converts it to data that makes sense to C3 for charting
 * @param  {obj} rawData         : Fuse's JSON Data
 * @return {obj} The parsed data
 */
FuseViz.prototype.parseData = function(rawData) {

  var parsedData = {};
  var arr = [];
  var freqSum;
  var columns = [];

  if (!_.isUndefined(rawData.items) && !_.isUndefined(rawData.items[0]) && rawData.items[0].items) {

    if (this.chartType === 'line' || this.chartType === 'bar' || this.chartType === 'area' || this.chartType === 'area-spline') {

      parsedData = _.map(rawData.items, function(item) {
        freqSum = 0;
        _.forEach(item.items, function(it) {
          freqSum += it.freq;
        });

        return {
          value: item.label,
          freq: freqSum
        };
      });
      return parsedData;
    }

    else if (this.chartType === 'pie' || this.chartType === 'donut') {
      parsedData.columns = [];

      _.forEach(rawData.items, function(item) {
        arr = [];
        arr.push(item.label);
        freqSum = 0;
        _.forEach(item.items, function(it) {
          freqSum += it.freq;
        });
        arr.push(freqSum);
        parsedData.columns.push(arr);
      });

      return parsedData;
    }
  }
  else {
    if (this.chartType === 'line' || this.chartType === 'bar' || this.chartType === 'area' || this.chartType === 'area-spline') {

      var xArr = _.pluck(rawData.items, 'value');
      var yArr = _.pluck(rawData.items, 'freq');

      if (this._isDateFacet(rawData)) {

        this.xType = 'timeseries';

        xArr = _.map(xArr, function(item) {
          return new Date(item);
        });
        xArr = _.sortBy(xArr, function(item) {
          return new Date(item);
        });

      }

      xArr.unshift(rawData.name);
      yArr.unshift(rawData.label);

      this.xs[rawData.label] = rawData.name;
      this.multipleTypes[rawData.label] = this.chartType;

      columns.push(xArr);
      columns.push(yArr);
      return columns;
    }

    else if (this.chartType === 'pie' || this.chartType === 'donut') {
      parsedData.columns = [];
      _.forEach(rawData.items, function(item) {
        arr.push(item.value);
        arr.push(item.freq);
        parsedData.columns.push(arr);
        arr = [];
      });

      return parsedData;
    }
  }

};

/**
 * Method to update the graph with new data
 * @param  {obj}    data  : New data to chart
 */
FuseViz.prototype.update = function(data) {
  if (_.isObject(data)) {
    this.rawData = data;
    this.facet = this.rawData.label;
    this.changeChartType(this.chartType);
  }
  else if (_.isString(data)) {
    var that = this;
    d3.json(data, function(response) {
      that.rawData = response;
      that.facet = that.rawData.label;
      that.changeChartType(that.chartType);
    });
  }
};

/**
 * Method to update the type of chart
 * @param  {string}    type  : 'pie', 'line', 'bar', 'donut'
 */
FuseViz.prototype.changeChartType = function(type) {
  if (this.currentData.length > 2) {
    console.log('Multiple datasets in chart, not possible to change type');
    return;
  }
  if ((type === 'donut' || type === 'pie') && _.keys(this.xs)
    .length > 1) {
    console.log('Charting multiple datasets with a type of: ' + type + ' , is invalid');
  }
  else {
    this.chartType = type;
    this.graph(this.parseData(this.rawData));
  }
};

/**
 * Method that loads another dataset into the chart instance
 * @param  {obj} data The dataset to load
 * @param  {string} type The type of chart to load the data as
 */
FuseViz.prototype.loadData = function(data, type) {

  var validTypes = [
    'bar',
    'line',
    'spline',
    'area',
    'area-spline'
  ];
  var newData;
  var ajaxData;
  var allData = [];

  if (_.contains(validTypes, type)) {
    if (_.isObject(data)) {
      newData = this.parseData(data);

      this.multipleTypes[newData[1][0]] = type;

      allData.push(this.currentData);
      allData.push(newData);
      allData = _.flatten(allData, true);

      this.currentData = allData;
      this.graph(allData);
    }
    else if (_.isString(data)) {

      var that = this;
      var url = data;

      d3.json(url, function(response) {
        _.forEach(that.currentData, function(column) {
          if (column[0] === response.label) {
            throw new that.Exception(response.label, 'Conflict of data, data with that name already loaded in the chart');
          }
        });

        ajaxData = that.parseData(response);

        that.multipleTypes[ajaxData[1][0]] = type;

        allData.push(that.currentData);
        allData.push(ajaxData);
        allData = _.flatten(allData, true);

        that.currentData = allData;

        that.graph(allData);
      });

    }
  }
  else {
    console.log('Invalid type: ' + type + ' ,to load mutiple sets of data, valid chart types are line, bar and area');
  }
};

/**
 * Method that graphs the data using c3
 * @param  {obj} parsedData The dataset to graph
 */
FuseViz.prototype.graph = function(parsedData) {
  var colorScale;
  var maxY;
  var that = this;
  var xDateFormat = d3.time.format('%Y-%m-%d');
  var tooltipDateFormat = d3.time.format('%Y-%m-%d %H:%m');
  var datasetColors = {};
  var c = 1;
  this.chart = null;

  switch (this.colorScheme) {
    case '10':
      colorScale = d3.scale.category10();
      break;
    case '20':
      colorScale = d3.scale.category20();
      break;
    case '20b':
      colorScale = d3.scale.category20b();
      break;
    case '20c':
      colorScale = d3.scale.category20c();
      break;
    default:
      colorScale = d3.scale.category20();
  }

  if (this.chartType === 'line' || this.chartType === 'bar' || this.chartType === 'area' || this.chartType === 'area-spline') {

    maxY = _.max(parsedData[1]);

    _.forEach(_.keys(this.xs), function(key) {
      datasetColors[key] = colorScale(c++);
    });

    this.chart = c3.generate({
      bindto: '#' + this.selector,
      data: {
        xs: this.xs,
        columns: parsedData,
        type: this.chartType,
        types: this.multipleTypes,
        labels: this.labels,
        colors: datasetColors,
        color: function(color, d) {
          if (that.xType === 'timeseries' || that.oneColor) {
            return color;
          }
          else {
            return colorScale(d.value);
          }
        }
      },
      subchart: {
        show: this.subchart
      },
      legend: {
        show: this.legend,
        position: this.legendPosition
      },
      padding: {
        top: 40,
        right: 0,
        bottom: 0,
        left: 80
      },
      axis: {
        x: {
          type: this.xType,
          tick: {
            fit: true,
            rotate: 60,
            // format: function(i) {
            //   var xLabel = parsedData[0][i+1];
            //   if (that.xType === 'timeseries') {
            //     return xDateFormat(i);
            //   } else {
            //     return xLabel;
            //   }
            // }
          }
        },
        y: {
          label: {
            text: 'Frequency',
            position: 'outer-middle',
          },
          tick: {
            format: function(d) {
              return d;
            }
          }
        }
      },
      grid: {
        x: {
          show: this.gridX
        },
        y: {
          show: this.gridY
        }
      },
      tooltip: {
        format: {
          title: function(x) {
            if (that.xType === 'timeseries') {
              return tooltipDateFormat(x);
            }
            else {
              return parsedData[0][x + 1];
            }
          }
        },
        show: this.tooltip
      },
      zoom: {
        enabled: this.zoom
      }
    });

  }
  else if (this.chartType === 'pie') {
    var i = 0;
    this.chart = c3.generate({
      bindto: '#' + this.selector,
      data: {
        columns: parsedData.columns,
        type: 'pie',
        color: function() {
          if (i >= parsedData.columns.length) {
            i = 0;
          }
          return colorScale(i++);
        }
      },
      legend: {
        show: this.legend,
        position: this.legendPosition
      },
      tooltip: {
        show: this.tooltip
      }
    });
  }
  else if (this.chartType === 'donut') {
    var j = 0;

    this.chart = c3.generate({
      bindto: '#' + this.selector,
      data: {
        columns: parsedData.columns,
        type: 'donut',
        color: function() {
          if (j >= parsedData.columns.length) {
            j = 0;
          }
          return colorScale(j++);
        }
      },
      donut: {
        title: this.facet || 'Facets'
      },
      legend: {
        show: this.legend,
        position: this.legendPosition
      },
      tooltip: {
        show: this.tooltip
      }
    });
  }

  // Add switcher
  if (!_.isUndefined(this.typeSwitcher) && this.currentData.length <= 2 && this.typeSwitcher) {
    this.addSwitcher();
  }
};

FuseViz.prototype.addSwitcher = function() {
  var options = [
    'Bar',
    'Line',
    'Area',
    'Area-Spline',
    'Donut',
    'Pie'
  ];

  var selectEl = document.createElement('select');
  var container = document.getElementById(this.selector);
  var selectedOption = this.chartType;
  var that = this;

  this.typeSwitcher = this.typeSwitcher || 'topRight';
  selectEl.id = 'typeSwitcher';
  selectEl.style.position = 'absolute';
  selectEl.style.top = '10px';
  selectEl.style.right = this.typeSwitcher === 'topRight' ? '10px' : 'auto';
  selectEl.style.left = this.typeSwitcher === 'topLeft' ? '10px' : 'auto';
  selectEl.style.fontSize = '10px';
  selectEl.style.width = '80px';
  selectEl.style.height = '20px';
  selectEl.style.background = '#eee';
  selectEl.style.border = '1px solid #ddd';
  container.style.position = 'relative';
  container.appendChild(selectEl);

  //Create and append the options
  for (var i = 0; i < options.length; i++) {
    var option = document.createElement('option');
    option.value = options[i].toLowerCase();
    option.text = options[i];
    selectEl.appendChild(option);
  }

  // Select the correct option
  selectEl.value = selectedOption;

  // Event listener
  selectEl.addEventListener('change', function() {
    selectedOption = selectEl.options[selectEl.selectedIndex].value;
    that.changeChartType(selectedOption);
  });

};

/**
 * Utility method to truncate a string at a given length and add a ellipsis
 * @param  {string} string  The string to truncate
 * @param  {number} n      The number of characters to leave the string at
 * @return {string}         The modified string
 */
FuseViz.prototype.trunc = function(string, n) {
  return string.length > n ? string.substr(0, n - 1) + '…' : string;
};

/**
 * Method to apply a range on the chart
 * @param {string} rangeFrom   A valid 'from' date string.
 * @param {string} rangeTo   A valid 'to' date string.
 */
FuseViz.prototype.setRange = function(rangeFrom, rangeTo) {
  var dateFrom = new Date(rangeFrom);
  var dateTo = new Date(rangeTo);
  var tempDate;
  var that = this;

  this.rangeData = _.clone(this.rawData);
  this.rangeData.items = [];

  _.forEach(this.rawData.items, function(item) {
    tempDate = new Date(item.value);

    if (tempDate >= dateFrom && tempDate <= dateTo) {
      that.rangeData.items.push(item);
    }
  });

  this.graph(this.parseData(this.rangeData));
};

/**
 * Method to get rid of the range restrictions
 */
FuseViz.prototype.resetRange = function() {
  this.rangeData = {};
  this.graph(this.currentData);
};

/**
 * Method to get the current instance configuration
 * @return {[obj]} The config object
 */
FuseViz.prototype.getConfig = function() {
  return {
    currentData: this.currentData,
    selector: this.selector,
    gridX: this.gridX,
    gridY: this.gridY,
    zoom: this.zoom,
    labels: this.labels,
    oneColor: this.oneColor,
    tooltip: this.tooltip,
    subchart: this.subchart,
    chartType: this.chartType,
    legend: this.legend,
    colorScheme: this.colorScheme,
    typeSwitcher: this.typeSwitcher
  };
};
