(function() {
  'use strict';
  var promise = {};
  var config = {};
  var config2 = {};

  var fuseInstance;
  var fuseInstanceAjax;
  var rangeInstance1;
  var rangeInstance2;

  var url = 'http://atlassian.qsensei.com:2080/facets/modified_date';
  var url2 = 'http://atlassian.qsensei.com:2080/facets/created_date';


  promise.testLoadingData = webServices.getJasonSample()
    .success(function(data) {
      config = {
        type: 'line',
        selector: 'chart',
        data: data.items[7],
        gridX: false,
        gridY: true,
        zoom: false,
        tooltip: true,
        typeSwitcher: 'topRight',
        colorScheme: '20',
        oneColor: true,
        legend: true
      };
      fuseInstance = new FuseViz(config);
      // fuseInstance.loadData(data.items[8], 'bar');

      var madeUpData = _.clone(data.items[8], true);
      madeUpData.name = 'updated_span';
      madeUpData.label = 'Updated Date';
      fuseInstance.loadData(madeUpData, 'line');

      fuseInstance.loadData(url2, 'line');

    });

  config2 = {
    type: 'line',
    selector: 'ajaxChart',
    data: url,
    gridX: false,
    gridY: true,
    zoom: false,
    tooltip: true,
    typeSwitcher: 'topRight',
    colorScheme: '20',
    oneColor: true,
    legend: true,
    legendPosition: 'right',
    labels: false
  };

  fuseInstanceAjax = new FuseViz(config2);

  setTimeout(function() {
    fuseInstanceAjax.update(url2);
  }, 2000);

  // Range
  promise.getJasonData = webServices.getJasonSample()
    .success(function(data) {

      config = {
        type: 'line',
        selector: 'rangeChart1',
        data: data.items[8],
        gridX: false,
        gridY: true,
        zoom: false,
        tooltip: true,
        typeSwitcher: 'topLeft',
        colorScheme: '20b' //'20', '20b', '20c'
      };

      rangeInstance1 = new FuseViz(config);
      rangeInstance1.setRange('2014-04-20', '2014-05-20');
    });

  promise.getJasonData = webServices.getJasonSample()
    .success(function(data) {
      config = {
        type: 'line',
        selector: 'rangeChart2',
        data: data.items[7],
        gridX: false,
        gridY: true,
        zoom: false,
        tooltip: true,
        typeSwitcher: 'topRight',
        colorScheme: '10' //'20', '20b', '20c'
      };

      rangeInstance2 = new FuseViz(config);
      rangeInstance2.setRange('2014-05-13', '2014-05-20');
      setTimeout(function() {
        rangeInstance2.resetRange();
      }, 3000);
      $('#configurationExample code')
        .text(JSON.stringify(rangeInstance2.getConfig(), null, 2));
    });

})();
