'use strict';
var path = require('path');
var gulp = require('gulp');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var gutil = require('gulp-util');
var less = require('gulp-less');
var csso = require('gulp-csso');
var autoprefixer = require('gulp-autoprefixer');
var changed = require('gulp-changed');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');


var jsDependencies = [
  'bower_components/d3/d3.js',
  'bower_components/c3/c3.js',
  'bower_components/lodash/dist/lodash.compat.js'
];

var cssDependencies = 'bower_components/c3/c3.css';

gulp.task('clean', function() {
  return gulp.src(['./css', 'build'], {read: false})
  .pipe(clean());
});

gulp.task('css', function() {
  return gulp.src('less/main.less')
  .pipe(changed('./css'))
  .pipe(less({
    paths: [ path.join(__dirname, 'less', 'includes') ],
    sourcemap: true
  }))
  .pipe(autoprefixer('last 2 versions'))
  .pipe(gulp.dest('./css'))
  .pipe(rename({suffix: '.min'}))
  .pipe(csso())
  .pipe(gulp.dest('./css'))
  .on('error', gutil.log);
});

gulp.task('jsVendor', function() {
  return gulp.src(jsDependencies)
  .pipe(gulp.dest('build/vendor/js'))
  .on('error', gutil.log);
});

gulp.task('cssVendor', function() {
  return gulp.src(cssDependencies)
  .pipe(gulp.dest('build/vendor/css'))
  .on('error', gutil.log);
});

gulp.task('scripts', function() {
  return gulp.src('scripts/FuseViz.js')
  .pipe(gulp.dest('build'))
  .pipe(uglify())
  .pipe(rename('FuseViz.min.js'))
  .pipe(gulp.dest('build'))
  .on('error', gutil.log);
});

gulp.task('build', ['clean', 'jsVendor', 'cssVendor', 'scripts']);

gulp.task('browser-sync', function() {
    browserSync.init(['css/*.css', 'scripts/**/*.js', 'index.html'], {
        server: {
            baseDir: './'
        },
        browser: 'google chrome canary',
        notify: false
    });
});

gulp.task('default', ['css', 'browser-sync'], function () {
    gulp.watch('less/**/*.less', ['css']);
});
